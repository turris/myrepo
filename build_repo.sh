#!/bin/sh
set -e

# TODO add some help and some argumenst parsing

BOARD=omnia
if [ "$1" = "omnia" ] || [ "$1" = "turris" ] || [ "$1" = "all" ]; then
	BOARD=$1
	shift
fi

# remove previous builds
rm -rf .omnia-sdk
rm -rf .turris-sdk

pull_sdk() {
	# Update only once a day
	if [ ! -e .$1-sdk-clean ] || [ $(expr $(date -u +%s) - $(stat -c %Z .$1-sdk-clean)) -gt 86400 ]; then
		rm -rf .$1-sdk-clean
		echo -e "\e[1;34mGetting current SDK for $1\e[0m"
		curl "$2" | tar -xjf -
		mv OpenWrt-SDK-* .$1-sdk-clean
	fi
	# Copy clean sdk
	rm -rf .$1-sdk
	rsync -aW --inplace --delete .$1-sdk-clean/ .$1-sdk/
}

# TODO these links won't be valid forever
if [ "$BOARD" = "omnia" ] || [ "$BOARD" = "all" ]; then
	pull_sdk omnia "https://repo.turris.cz/omnia/OpenWrt-SDK-mvebu_gcc-4.8-linaro_musl-1.1.15_eabi.Linux-x86_64.tar.bz2"
fi
if [ "$BOARD" = "turris" ] || [ "$BOARD" = "all" ]; then
	pull_sdk turris "https://repo.turris.cz/turris/OpenWrt-SDK-mpc85xx-p2020-nand_gcc-4.8-linaro_uClibc-0.9.33.2.Linux-x86_64.tar.bz2"
fi


echo -e "\e[1;34mCopy packages\e[0m"
for d in *; do
	[ -f $d/Makefile ] || continue
	echo -e "\e[1;35mpackage: $d\e[0m"
	[ -d .omnia-sdk ] && cp -r $d .omnia-sdk/package/
	[ -d .turris-sdk ] && cp -r $d .turris-sdk/package/
done


deploy_pkgauto() {
	cat >"$1/include/pkgauto.mk" <<EOF
ifneq (\$(PKG_SOURCE_PROTO),git)
\$(error Only supported protocol is git)
endif
ifndef PKG_NAME
\$(error You have to define PKG_NAME before pkgauto.mk include)
endif
ifndef PKG_SOURCE_URL
\$(error You have to define PKG_SOURCE_URL before pkgauto.mk include)
endif
ifndef PKG_SOURCE_BRANCH
\$(error You have to define PKG_SOURCE_BRANCH before pkgauto.mk include)
endif

TMP_REPO_PATH=.$PWD/myrepo_\$(PKG_NAME)

GIT_ARGS=--git-dir='\$(TMP_REPO_PATH)' --bare

\$(shell \
	if [ ! -d "\$(TMP_REPO_PATH)" ]; then \
		git clone --bare "\$(PKG_SOURCE_URL)" "\$(TMP_REPO_PATH)"; \
	else \
		git \$(GIT_ARGS) fetch "\$(PKG_SOURCE_URL)"; \
	fi \
)

PKG_SOURCE_VERSION:=\$(shell git \$(GIT_ARGS) rev-parse \$(PKG_SOURCE_BRANCH))
PKG_VERSION:=\$(shell git \$(GIT_ARGS) describe --abbrev=0 --tags \$(PKG_SOURCE_BRANCH) | sed 's/^v//')

ifeq (\$(PKG_VERSION),)
# No version found so we use 9999 instead and count commits since initial commit.
PKG_VERSION:=9999
PKG_RELEASE:=\$(shell git \$(GIT_ARGS) rev-list --count \$(PKG_SOURCE_VERSION))
else
PKG_RELEASE:=\$(shell git \$(GIT_ARGS) rev-list --count v\$(PKG_VERSION)..\$(PKG_SOURCE_VERSION))
PKG_VERSION:=\$(PKG_VERSION).9999
endif

PKG_SOURCE:=\$(PKG_NAME)-\$(PKG_SOURCE_VERSION).tar.gz
PKG_SOURCE_SUBDIR:=\$(PKG_NAME)
PKG_BUILD_DIR:=\$(BUILD_DIR)/\$(PKG_NAME)
HOST_BUILD_DIR:=\$(BUILD_DIR_HOST)/\$(PKG_NAME)

# TODO drop TMP_REPO_PATH and GIT_ARGS
EOF
}

[ -d .omnia-sdk ] && deploy_pkgauto .omnia-sdk
[ -d .turris-sdk ] && deploy_pkgauto .turris-sdk


if [ -f CONFIG ]; then
	echo -e "\e[1;34mApply CONFIG\e[0m"
	([ "$BOARD" = "omnia" ] || [ "$BOARD" = "all" ]) && \
	   cat CONFIG >> .omnia-sdk/.config
	([ "$BOARD" = "turris" ] || [ "$BOARD" = "all" ]) && \
	   cat CONFIG >> .turris-sdk/.config
fi

if [ "$BOARD" = "omnia" ] || [ "$BOARD" = "all" ]; then
	echo -e "\e[1;34mBuilding omnia\e[0m"
	(cd .omnia-sdk; make "$@")
fi
if [ "$BOARD" = "turris" ] || [ "$BOARD" = "all" ]; then
	echo -e "\e[1;34mBuilding turris\e[0m"
	(cd .turris-sdk; make "$@")
fi


pull_out() {
	echo -e "\e[1;34mMove and sign $1\e[0m"
	[ ! -d ./repo/$1 ] || rm -rf ./repo/$1
	mkdir -p repo
	cp -r .$1-sdk/bin/$2/packages/base ./repo/$1
	./.$1-sdk/staging_dir/host/bin/usign -S -s key.sec -m "repo/$1/Packages"
}

if [ "$BOARD" = "omnia" ] || [ "$BOARD" = "all" ]; then
	pull_out omnia mvebu-musl
fi
if [ "$BOARD" = "turris" ] || [ "$BOARD" = "all" ]; then
	pull_out turris mpc85xx
fi
